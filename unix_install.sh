mkdir -p ~/.local/bin
ln -sf $PWD/respode.py ~/.local/bin/respode

echo "respode.py is now symbolically linked to ~/.local/bin/respode"
echo "Make sure ~/.local/bin is a part of your \$PATH variable and you can use the 'respode' command from the terminal."
