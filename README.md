Respode
---

Respode = Response + Code

Simple CLI tool for getting response codes from URLs

## Installation

If you are new to the commandline, there is nothing wrong with running this program by using `python3 respode.py`.

However, it can be more intuitive to use a simple `respode` command instead.

If you are on a Linux, Mac, or BSD, you can use the `unix_install.sh` script to install. I have only tested it on the a Linux machine but it should work elsewhere. Please review it before running.

**How to Install**:

`sh unix_install.sh`

*Note: You should also make sure "/home/YOUR_USERNAME/.local/bin" is in your $PATH variable. This can be placed in your .bash_profile, .bashrc, .zshrc, or anything similar.*

Ex. PATH=/home/doug/.local/bin:$PATH

**How to Uninstall**:

`sh unix_uninstall.sh`

## Features:

- Can process one or many URLs
- Support checking a textfile of URLs using the `-f` flag.
- Basic commenting using `#` symbol in textfiles
- When a 3xx code is returned, Respode shows the redirection path

## Example usages:

- python3 respode.py https://google.com/fake-page
- python3 respode.py https://google.com http://reddit.com
- python3

## Dependencies

None!

The Python Requests library should come by default on most systems.
