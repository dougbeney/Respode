#!/usr/bin/env python3

import re
import sys
import requests
from pprint import pprint

VERSION = 'v0.0.1'
urls    = []

def add_urls_from_file(fname):
	with open(fname) as f:
		for line_number,url in enumerate(f.readlines()):
			urls.append({
				"url": url.strip(),
				"line_number": line_number
			})

args  = sys.argv
del args[0]
largs = len(args)
if largs == 0:
	print("Respode -", VERSION)
	print("Example Usages:")
	print("$ respode")
	print("$ respode https://google.com")
	print("$ respode -f urls.txt")
	sys.exit(0)
else:
	for index,arg in enumerate(args):
		if arg == "-f":
			if index+1 < largs:
				add_urls_from_file(args[index+1])
			else:
				print("Syntax error in command.")
				sys.exit(0)
			break
		else:
			urls.append({
				"url": arg,
				"line_number": None
			})

# Organize URL objects
urls = sorted(urls, key=lambda k: k['url'])

def get(url, allow_redirects=False):
	request = None
	try:
		request = requests.get(url, allow_redirects=allow_redirects)
	except Exception as e:
		print(e)
		print("[Error] Can't connect to", url)
		sys.exit(0)
	return request

def status_code(url):
	g = get(url)
	try:
		status = g.status_code
		redirect_location = g.raw.get_redirect_location()
		if redirect_location:
			return str(g.status_code) + " -> " + redirect_location
		return g.status_code
	except Exception as e:
		print("[Warning]", e)
	return None

if not urls:
	print("No urls to process")
	sys.exit(0)

domain_list = []

for index,urlObject in enumerate(urls):
	url = urlObject['url']
	ln  = urlObject['line_number']
	if not url[0] == "#":
		if ln:
			ln_str = " Line #" + str(ln) + ".)"
		else:
			ln_str = ""
		re_pattern = r'(https?:\/\/)?(www\.)?[^\/]+\.\w+\/?' # Regex pattern to remove root domain
		domain = re.search(re_pattern, url).group(0)
		domain = re.sub(r'\/$', '', domain)
		if not domain in domain_list:
			print(domain)
		print("*", re.sub(re_pattern, '/', repr(url), 1), "=", status_code(url), ln_str)
		domain_list.append(domain)

